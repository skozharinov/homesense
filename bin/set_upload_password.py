import json
import os


Import("env")

data_dir = env["PROJECT_DATA_DIR"]
config_file = os.path.join(data_dir, "config.json")

with open(config_file, "r") as f:
    config = json.load(f)

env.Replace(UPLOAD_FLAGS=f"--auth={config["ota"]["password"]}")

# HomeSense

ESP8266 and DHT*XX*-based air temperature and humidity sensor for Home Assistant

## Features

- MQTT connectivity tailored for [Home Assistant](https://www.home-assistant.io/)
- Configurable DHT*XX* sensor support
- Home Assistant autodiscovery
- Availability messages
- Handling of Home Assistant birth and death
- OTA updates via `espota`
- Configuration via a configuration file uploaded to board's filesystem

## Configuration

Configuration is done in JSON,
and it should be uploaded to board's LittleFS filesystem as `/config.json`;

Here is an example configuration:

```json
{
  "log": {
    "level": "DEBUG"
  },
  "sensor": {
    "pin": 4,
    "kind": "DHT22"
  },
  "wifi": {
    "ssid": "MyAwesomeWiFi",
    "password": "SuperSecretPassword1"
  },
  "mqtt": {
    "host": "192.168.1.1",
    "port": 1883,
    "user": "homesenseuser",
    "password": "SuperSecretPassword2"
  },
  "ota": {
    "password": "SuperSecretPassword3"
  },
  "name": "homesense"
}
```

You can find default configuration values in [Parameters.hpp](include/Parameters.hpp) file.

## License

Copyright (C) 2024 Sergey Kozharinov

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.
If not, see <https://www.gnu.org/licenses/>.

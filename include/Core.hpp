#pragma once

#include "Led.hpp"
#include "Logger.hpp"
#include "Parameters.hpp"

#include <PubSubClient.h>

#include <ArduinoOTA.h>

#include <WiFiClient.h>

#include <ArduinoJson.hpp>

#include <string_view>

namespace HomeSense {
class Core {
public:
  virtual ~Core() = default;

  virtual void doSetup();
  virtual void doLoop();

protected:
  Parameters m_parameters;

  Logger m_logger;
  Led m_led;

  std::string m_availabilityTopic;

protected:
  static std::string craftTopicName(std::string_view component, std::string_view objectId, std::string_view purpose);

  explicit Core(Parameters parameters);

  [[nodiscard]] ArduinoJson::JsonDocument makeDeviceJson() const;

  void publishJson(std::string_view topic, const ArduinoJson::JsonDocument &document);

  void publishText(std::string_view topic, std::string_view text);

  [[nodiscard]] bool isReady() const;

  void subscribe(std::string_view topic);

  virtual void publishDiscoveryMessages();

  virtual void publishAvailability(bool isOnline);

  virtual void doSubscriptionCallback(std::string_view topic, std::string_view payload);

  virtual void doReconnectCallback();

private:
  static constexpr std::uint32_t OTA_MESSAGE_INTERVAL_MS = 1000;
  static constexpr std::uint32_t MESSENGER_CONNECTION_INTERVAL_MS = 1000;

  static constexpr std::uint32_t NETWORK_CONNECTION_LED_PERIOD_MS = 500;

  WiFiClient m_networkClient;
  ArduinoOTAClass m_ota;
  PubSubClient m_messenger;

  std::uint32_t m_previousMessengerConnectionAttempt = 0;

  bool m_isBirthMessageReceived = false;

private:
  bool setupNetwork();
  bool setupMessenger();

  bool ensureNetworkConnectivity();
  bool ensureMessengerConnectivity();
};
} // namespace HomeSense

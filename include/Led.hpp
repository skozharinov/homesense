#pragma once

#include <cstdint>

namespace HomeSense {
class Led {
public:
  explicit Led(std::uint8_t pin, bool isInverted = false);

  void doSetup();

  void turnOn();
  void turnOff();

  void toggle();

  [[nodiscard]] bool isOn() const;
  [[nodiscard]] bool isOff() const;

private:
  std::uint8_t m_pin;

  std::uint8_t m_onState;
  std::uint8_t m_offState;
};
} // namespace HomeSense

#pragma once

#include <HardwareSerial.h>

#include <string_view>

namespace HomeSense {
enum class LogLevel : std::uint8_t {
  NONE = 10,
  FATAL = 5,
  ERROR = 4,
  WARNING = 3,
  INFO = 2,
  DEBUG = 1,
};

std::string_view getLevelStr(LogLevel level);

class Logger {
public:
  explicit Logger(LogLevel level);

  void doSetup();

  template <class... Args> inline void operator()(LogLevel level, Args &&...args) {
    if (level < m_level || level == LogLevel::NONE)
      return;

    impl("[", millis(), "] ", getLevelStr(level), ": ", std::forward<Args>(args)...);
    m_serial.println();
    m_serial.flush();
  }

private:
  HardwareSerial m_serial{0};
  LogLevel m_level;

private:
  template <class T, class... Args> inline void impl(T &&part, Args &&...args) {
    if constexpr (std::is_same_v<std::decay_t<T>, std::string_view> || std::is_same_v<std::decay_t<T>, std::string>) {
      m_serial.print(part.data());
    } else {
      m_serial.print(std::forward<T>(part));
    }

    if constexpr (sizeof...(args) > 0) {
      impl<Args...>(std::forward<Args>(args)...);
    }
  }
};
} // namespace HomeSense

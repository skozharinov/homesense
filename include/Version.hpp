#pragma once

#define HOMESENSE_VERSION_MAJOR 0
#define HOMESENSE_VERSION_MINOR 4
#define HOMESENSE_VERSION_PATCH 3

#define HOMESENSE_VERSION_IMPL(major, minor, patch) #major "." #minor "." #patch
#define HOMESENSE_VERSION(major, minor, patch) HOMESENSE_VERSION_IMPL(major, minor, patch)

#define HOMESENSE_VERSION_FULL                                                                                         \
  HOMESENSE_VERSION(HOMESENSE_VERSION_MAJOR, HOMESENSE_VERSION_MINOR, HOMESENSE_VERSION_PATCH)

namespace HomeSense {
constexpr std::string_view getProjectName() { return "HomeSense"; }

constexpr std::string_view getProjectVersion() { return HOMESENSE_VERSION_FULL; }

constexpr std::string_view getProjectFlavor() {
#ifdef HOMESENSE_SENSOR_TARGET
  return "Sensor";
#endif

#ifdef HOMESENSE_DISPLAY_TARGET
  return "Display";
#endif
}

constexpr std::string_view getHardwarePlatform() {
#ifdef ESP8266
  return "ESP8266";
#endif
#ifdef ESP32
  return "ESP32";
#endif
}
} // namespace HomeSense

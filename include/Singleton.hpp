#pragma once

#include <memory>

namespace HomeSense {
template <class T> class Singleton {
public:
  struct PrivatePass {
  private:
    explicit constexpr PrivatePass() = default;
    friend class Singleton<T>;
  };

public:
  explicit constexpr Singleton(PrivatePass) {}

  Singleton(const Singleton &) = delete;
  Singleton(Singleton &&) = delete;
  void operator=(const Singleton &) = delete;
  void operator=(Singleton &&) = delete;

  template <class... Args> static inline void createInstance(Args &&...args) {
    s_instance = std::make_unique<T>(PrivatePass{}, std::forward<Args>(args)...);
  }

  static inline T &getInstance() { return *s_instance; }

private:
  static inline std::unique_ptr<T> s_instance;
};
} // namespace HomeSense

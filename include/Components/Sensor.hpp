#pragma once

#include "Core.hpp"
#include "Singleton.hpp"

#include <DHT.h>

namespace HomeSense {
class Sensor final : public Core, public Singleton<Sensor> {
public:
  explicit Sensor(PrivatePass, Parameters parameters);

  void doSetup() override;
  void doLoop() override;

private:
  static constexpr std::uint32_t MIN_MEASUREMENT_INTERVAL_MS = 2 * 1000;
  static constexpr std::uint32_t MAX_MEASUREMENT_INTERVAL_MS = 3600 * 1000;

  static constexpr std::uint32_t SENSOR_FAILURE_LED_PERIOD_MS = 100;

  DHT m_sensor;

  std::uint32_t m_previousMeasurementPointMs = 0;
  std::uint32_t m_measurementIntervalMs = 60000;

  float m_lastReadHumidity;
  float m_lastReadTemperature;

  std::string m_temperatureDiscoveryTopic;
  std::string m_humidityDiscoveryTopic;
  std::string m_measurementIntervalDiscoveryTopic;
  std::string m_measurementIntervalCommandTopic;
  std::string m_stateTopic;

  bool m_isLastSensorReadSuccessful = false;

private:
  void publishDiscoveryMessages() override;

  void publishAvailability(bool isOnline) override;

  void doSubscriptionCallback(std::string_view topic, std::string_view payload) override;

  void doReconnectCallback() override;

  void setupSensor();

  void publishTemperatureDiscoveryMessage();

  void publishHumidityDiscoveryMessage();

  void publishMeasurementIntervalDiscoveryMessage();

  void readSensorState();

  void publishStateMessage();
};
} // namespace HomeSense

#pragma once

#include "Core.hpp"
#include "Singleton.hpp"

#include <Adafruit_SSD1306.h>

namespace HomeSense {
class Display final : public Core, public Singleton<Display> {
public:
  explicit Display(PrivatePass, Parameters parameters);

  void doSetup() override;
  void doLoop() override;

private:
  TwoWire m_wire;
  Adafruit_SSD1306 m_display;

  std::string m_discoveryTopic;
  std::string m_commandTopic;
  std::string m_stateTopic;

private:
  void setupDisplay();

  void displayCenteredText(std::string_view str);

  void publishState(std::string_view str);

  void publishDiscoveryMessages() override;

  void doSubscriptionCallback(std::string_view topic, std::string_view payload) override;

  void doReconnectCallback() override;
};
} // namespace HomeSense

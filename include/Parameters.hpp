#pragma once

#include "Logger.hpp"

#ifdef HOMESENSE_SENSOR_TARGET
#include <DHT.h>
#endif

#include <cstdint>
#include <string>

namespace HomeSense {
struct LogParameters {
  LogLevel level = LogLevel::WARNING;
};

struct WifiParameters {
  std::string ssid = "IOT";
  std::string password;
};

struct MqttParameters {
  std::string host = "192.168.0.1";
  std::uint16_t port = 1883;
  std::string user;
  std::string password;
};

struct OtaParameters {
  std::string password;
};

#ifdef HOMESENSE_SENSOR_TARGET
enum class SensorKind : std::uint8_t {
  NONE = 0,
  DHT11 = ::DHT11,
  DHT12 = ::DHT12,
  DHT21 = ::DHT21,
  DHT22 = ::DHT22,
};

struct SensorParameters {
  std::uint8_t pin = 0;
  SensorKind kind = SensorKind::DHT11;
  std::int32_t measurementIntervalMs = -1;
};
#endif

#ifdef HOMESENSE_DISPLAY_TARGET
struct DisplayParameters {
  std::uint8_t dataPin = 1;
  std::uint8_t clockPin = 2;
  std::uint8_t width = 128;
  std::uint8_t height = 32;
};
#endif

struct Parameters {
  std::string name = "homesense0";

  LogParameters log;
  WifiParameters wifi;
  MqttParameters mqtt;
  OtaParameters ota;

#ifdef HOMESENSE_SENSOR_TARGET
  SensorParameters sensor;
#endif
#ifdef HOMESENSE_DISPLAY_TARGET
  DisplayParameters display;
#endif
};

Parameters readParameters();
} // namespace HomeSense

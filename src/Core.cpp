#include "Core.hpp"

#include "Version.hpp"

#ifdef ESP8266
#include <ESP8266WiFi.h>
#endif

#ifdef ESP32
#include <WiFi.h>
#endif

using namespace HomeSense;

std::string Core::craftTopicName(std::string_view component, std::string_view objectId, std::string_view purpose) {
  constexpr std::string_view SEPARATOR = "/";

  std::string topic = "homeassistant";
  topic.append(SEPARATOR);
  topic.append(component);
  topic.append(SEPARATOR);
  topic.append(objectId);
  topic.append(SEPARATOR);
  topic.append(purpose);

  return topic;
}

Core::Core(Parameters parameters)
    : m_parameters(std::move(parameters)), m_logger(m_parameters.log.level), m_led(LED_BUILTIN, true),
      m_availabilityTopic(craftTopicName("sensor", m_parameters.name, "status")) {}

void Core::doSetup() {
  m_logger.doSetup();

  m_logger(LogLevel::INFO, "Starting ", getProjectName(), " ", getProjectFlavor(), " v", getProjectVersion(), " on ",
           getHardwarePlatform());

  m_led.doSetup();

  m_ota.onStart([this]() {
    m_logger(LogLevel::WARNING, "OTA update started");
    publishAvailability(false);
  });
  m_ota.onError([this](ota_error_t error) {
    switch (error) {
    case OTA_AUTH_ERROR:
      m_logger(LogLevel::WARNING, "OTA authorization failure");
      break;
    case OTA_BEGIN_ERROR:
      m_logger(LogLevel::WARNING, "OTA start failure");
      break;
    case OTA_CONNECT_ERROR:
      m_logger(LogLevel::WARNING, "OTA connection failure");
      break;
    case OTA_RECEIVE_ERROR:
      m_logger(LogLevel::WARNING, "OTA receive failure");
      break;
    case OTA_END_ERROR:
      m_logger(LogLevel::WARNING, "OTA end failure");
      break;
    }

    publishAvailability(true);
  });
  m_ota.onProgress([this, previousProgressMessage = std::uint32_t{}](std::size_t progress, std::size_t size) mutable {
    std::uint32_t currentProgressMessage = millis();

    if (currentProgressMessage - previousProgressMessage < OTA_MESSAGE_INTERVAL_MS) {
      return;
    }

    m_logger(LogLevel::DEBUG, "OTA progress: ", progress, "/", size, " bytes");
    previousProgressMessage = currentProgressMessage;
  });
  m_ota.onEnd([this]() {
    m_logger(LogLevel::WARNING, "OTA update finished");
    m_messenger.disconnect();
  });

  m_messenger.setCallback([this](const char *topic, const std::uint8_t *payload, std::size_t length) {
    const std::string_view topicView = topic;
    const std::string payloadView(reinterpret_cast<const char *>(payload), length);

    doSubscriptionCallback(topicView, payloadView);
  });
}

void Core::doLoop() {
  std::uint32_t currentLoopStart = millis();

  if (!ensureNetworkConnectivity()) {
    return ESP.restart();
  }

  m_ota.handle();

  if (currentLoopStart - m_previousMessengerConnectionAttempt < MESSENGER_CONNECTION_INTERVAL_MS) {
    return;
  }

  if (!ensureMessengerConnectivity()) {
    m_previousMessengerConnectionAttempt = currentLoopStart;
    return;
  }

  m_messenger.loop();

  if (m_isBirthMessageReceived) {
    publishDiscoveryMessages();
    publishAvailability(true);
    m_isBirthMessageReceived = false;
  }
}

bool Core::setupNetwork() {
  m_logger(LogLevel::INFO, "Attempting connection to SSID ", m_parameters.wifi.ssid);
  WiFi.begin(m_parameters.wifi.ssid.data(), m_parameters.wifi.password.data());

  while (WiFi.status() == WL_DISCONNECTED) {
    m_led.toggle();
    delay(NETWORK_CONNECTION_LED_PERIOD_MS / 2);
  }

  m_led.turnOff();

  switch (WiFi.status()) {
  case WL_CONNECTED: {
    m_logger(LogLevel::INFO, "Connected to SSID ", m_parameters.wifi.ssid, ", IP: ", WiFi.localIP());

    const std::string hostname = m_parameters.name + ".homesense";

    WiFi.setHostname(hostname.data());
    WiFi.setAutoReconnect(true);

    return true;
  }

  case WL_NO_SSID_AVAIL:
    m_logger(LogLevel::FATAL, "SSID ", m_parameters.wifi.ssid, " is not available");
    return false;

#ifdef ESP8266
  case WL_WRONG_PASSWORD:
    m_logger(LogLevel::FATAL, "Wrong Wi-Fi password specified");
    return false;
#endif

  default:
    m_logger(LogLevel::FATAL, "Unknown Wi-Fi error: ", static_cast<std::underlying_type_t<wl_status_t>>(WiFi.status()));
    return false;
  }
}

bool Core::setupMessenger() {
  m_logger(LogLevel::INFO, "Attempting connection to broker ", m_parameters.mqtt.host, ":", m_parameters.mqtt.port);

  m_messenger.setClient(m_networkClient);
  m_messenger.setServer(m_parameters.mqtt.host.data(), m_parameters.mqtt.port);

  const std::string clientId = "ESP8266_" + m_parameters.name;

  m_messenger.connect(clientId.data(), m_parameters.mqtt.user.data(), m_parameters.mqtt.password.data(),
                      m_availabilityTopic.data(), 0, false, "offline");

  switch (m_messenger.state()) {
  case MQTT_CONNECTED:
    m_logger(LogLevel::INFO, "Connected to broker ", m_parameters.mqtt.host, ":", m_parameters.mqtt.port);
    return true;

  case MQTT_CONNECTION_TIMEOUT:
    m_logger(LogLevel::FATAL, "Broker connection timed out");
    return false;

  case MQTT_CONNECT_FAILED:
    m_logger(LogLevel::FATAL, "Broker connection failed");
    return false;

  case MQTT_CONNECT_BAD_PROTOCOL:
    m_logger(LogLevel::FATAL, "Broker does not support requested version of MQTT");
    return false;

  case MQTT_CONNECT_BAD_CLIENT_ID:
    m_logger(LogLevel::FATAL, "Broker rejected identifier");
    return false;

  case MQTT_CONNECT_UNAVAILABLE:
    m_logger(LogLevel::FATAL, "Broker is not available");
    return false;

  case MQTT_CONNECT_BAD_CREDENTIALS:
    m_logger(LogLevel::FATAL, "Wrong broker credentials specified");
    return false;

  case MQTT_CONNECT_UNAUTHORIZED:
    m_logger(LogLevel::FATAL, "Broker rejected connection");
    return false;

  default:
    m_logger(LogLevel::FATAL, "Unknown broker error");
    return false;
  }
}

bool Core::ensureNetworkConnectivity() {
  if (WiFi.status() != WL_CONNECTED) {
    if (!setupNetwork())
      return false;

    m_ota.setPassword(m_parameters.ota.password.data());
    m_ota.begin();
  }

  return true;
}

bool Core::ensureMessengerConnectivity() {
  if (m_messenger.state() != MQTT_CONNECTED) {
    if (!setupMessenger())
      return false;

    doReconnectCallback();
  }

  return true;
}

ArduinoJson::JsonDocument Core::makeDeviceJson() const {
  ArduinoJson::JsonDocument connectionTuple;
  connectionTuple[0] = "mac";
  connectionTuple[1] = WiFi.macAddress();

  ArduinoJson::JsonDocument connectionsJson;
  connectionsJson[0] = connectionTuple;

  ArduinoJson::JsonDocument config;

  std::string deviceName(getProjectName());
  deviceName += " ";
  deviceName += getHardwarePlatform();
  deviceName += " ";
  deviceName += getProjectFlavor();

  config["name"] = deviceName;
  config["manufacturer"] = getProjectName();
  config["model"] = getProjectFlavor();
  config["sw_version"] = getProjectVersion();
  config["connections"] = connectionsJson;

  return config;
}

void Core::publishJson(std::string_view topic, const ArduinoJson::JsonDocument &document) {
  m_logger(LogLevel::DEBUG, "Sending a JSON message to ", topic);

  m_messenger.beginPublish(topic.data(), ArduinoJson::measureJson(document), false);
  ArduinoJson::serializeJson(document, m_messenger);
  m_messenger.endPublish();
}

void Core::publishText(std::string_view topic, std::string_view text) {
  m_logger(LogLevel::DEBUG, "Sending a text message of length ", text.size(), " to ", topic);

  m_messenger.publish(topic.data(), text.data(), false);
}

bool Core::isReady() const { return const_cast<PubSubClient &>(m_messenger).state() == MQTT_CONNECTED; }

void Core::subscribe(std::string_view topic) { m_messenger.subscribe(topic.data()); }

void Core::publishDiscoveryMessages() {}

void Core::publishAvailability(bool isOnline) {
  constexpr std::string_view ONLINE_PAYLOAD = "online";
  constexpr std::string_view OFFLINE_PAYLOAD = "offline";

  m_logger(LogLevel::INFO, "Sending an availability message");

  if (isOnline) {
    publishText(m_availabilityTopic, ONLINE_PAYLOAD);
  } else {
    publishText(m_availabilityTopic, OFFLINE_PAYLOAD);
  }
}

void Core::doSubscriptionCallback(std::string_view topic, std::string_view payload) {
  m_logger(LogLevel::DEBUG, "Received a message in ", topic, " of length ", payload.size());

  constexpr std::string_view TOPIC = "homeassistant/status";

  constexpr std::string_view ONLINE_PAYLOAD = "online";
  constexpr std::string_view OFFLINE_PAYLOAD = "offline";

  if (topic == TOPIC) {
    if (payload == ONLINE_PAYLOAD) {
      m_logger(LogLevel::INFO, "Received birth message");
      m_isBirthMessageReceived = true;
    }

    if (payload == OFFLINE_PAYLOAD) {
      m_logger(LogLevel::INFO, "Received death message");
    }
  }
}

void Core::doReconnectCallback() {
  subscribe("homeassistant/status");
  m_logger(LogLevel::INFO, "Sending discovery messages");
  publishDiscoveryMessages();
}

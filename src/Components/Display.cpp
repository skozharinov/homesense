#include "Components/Display.hpp"

using namespace HomeSense;

Display::Display(PrivatePass pass, Parameters parameters)
    : Core(std::move(parameters)), Singleton<Display>(pass),
      m_display(m_parameters.display.width, m_parameters.display.height, &m_wire),
      m_discoveryTopic(craftTopicName("text", m_parameters.name, "config")),
      m_commandTopic(craftTopicName("text", m_parameters.name, "cmd")),
      m_stateTopic(craftTopicName("text", m_parameters.name, "state")) {}

void Display::doSetup() {
  Core::doSetup();
  setupDisplay();
}

void Display::doLoop() { Core::doLoop(); }

void Display::setupDisplay() {
  m_wire.begin(m_parameters.display.dataPin, m_parameters.display.clockPin);

  if (!m_display.begin()) {
    m_logger(LogLevel::FATAL, "Failed to allocate display buffer");
    return ESP.restart();
  }

  m_display.display();
}

void Display::displayCenteredText(std::string_view str) {
  m_logger(LogLevel::INFO, "Displaying text of length ", str.size());

  m_display.clearDisplay();

  std::int16_t x = 0;
  std::int16_t y = 0;
  std::uint16_t w = 0;
  std::uint16_t h = 0;

  m_display.setTextSize(1);
  m_display.setCursor(x, y);
  m_display.getTextBounds(str.data(), x, y, &x, &y, &w, &h);
  m_logger(LogLevel::DEBUG, "Calculated base text size at (", x, "; ", y, "): ", w, "x", h);

  if (w > m_parameters.display.width || h > m_parameters.display.height) {
    m_logger(LogLevel::WARNING, "Text will be clipped");
  }

  const std::uint16_t widthSize = static_cast<std::uint16_t>(m_parameters.display.width) / w;
  const std::uint16_t heightSize = static_cast<std::uint16_t>(m_parameters.display.height) / h;

  const std::uint16_t size = std::max(static_cast<std::uint16_t>(1), std::min(widthSize, heightSize));
  m_logger(LogLevel::DEBUG, "Calculated a size of ", size);

  const std::uint16_t remainingWidth =
      m_parameters.display.width -
      std::min(static_cast<std::uint16_t>(w * size), static_cast<std::uint16_t>(m_parameters.display.width));
  const std::uint16_t remainingHeight =
      m_parameters.display.height -
      std::min(static_cast<std::uint16_t>(h * size), static_cast<std::uint16_t>(m_parameters.display.height));

  const std::int16_t cursorX = remainingWidth / 2;
  const std::int16_t cursorY = remainingHeight / 2;

  m_logger(LogLevel::DEBUG, "Calculated a cursor position of (", cursorX, "; ", cursorY, ")");

  m_display.setTextSize(size);
  m_display.setTextColor(WHITE);
  m_display.setCursor(cursorX, cursorY);

  m_display.print(str.data());
  m_display.display();
}

void Display::publishState(std::string_view str) { publishText(m_stateTopic, str); }

void Display::publishDiscoveryMessages() {
  Core::publishDiscoveryMessages();

  m_logger(LogLevel::INFO, "Sending a discovery message");

  constexpr std::uint16_t CHAR_WIDTH = 6;
  constexpr std::uint16_t CHAR_HEIGHT = 8;

  const std::uint16_t horizontalCharsLimit = m_parameters.display.width / CHAR_WIDTH;
  const std::uint16_t verticalCharsLimit = m_parameters.display.height / CHAR_HEIGHT;

  ArduinoJson::JsonDocument config;
  config["name"] = "Text";
  config["unique_id"] = m_parameters.name;
  config["object_id"] = m_parameters.name;
  config["availability_topic"] = m_availabilityTopic;
  config["command_topic"] = m_commandTopic;
  config["state_topic"] = m_stateTopic;
  config["force_update"] = true;
  config["device"] = makeDeviceJson();
  config["max"] = horizontalCharsLimit * verticalCharsLimit;

  publishJson(m_discoveryTopic, config);

  delay(500);
  publishAvailability(true);
  publishState("");
}

void Display::doSubscriptionCallback(std::string_view topic, std::string_view payload) {
  Core::doSubscriptionCallback(topic, payload);

  if (topic == m_commandTopic) {
    displayCenteredText(payload);
    delay(1000);
    publishState(payload);
  }
}

void Display::doReconnectCallback() {
  Core::doReconnectCallback();
  subscribe(m_commandTopic);
}

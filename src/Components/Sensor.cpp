#include "Components/Sensor.hpp"

#include <charconv>

using namespace HomeSense;

Sensor::Sensor(PrivatePass pass, Parameters parameters)
    : Core(std::move(parameters)), Singleton<Sensor>(pass),
      m_sensor(parameters.sensor.pin, static_cast<std::underlying_type_t<SensorKind>>(parameters.sensor.kind), 12),
      m_temperatureDiscoveryTopic(craftTopicName("sensor", m_parameters.name + "_temperature", "config")),
      m_humidityDiscoveryTopic(craftTopicName("sensor", m_parameters.name + "_humidity", "config")),
      m_measurementIntervalDiscoveryTopic(
          craftTopicName("number", m_parameters.name + "_measurement_interval", "config")),
      m_measurementIntervalCommandTopic(craftTopicName("number", m_parameters.name + "_measurement_interval", "cmd")),
      m_stateTopic(craftTopicName("sensor", m_parameters.name, "state")) {}

void Sensor::doSetup() {
  Core::doSetup();
  setupSensor();

  if (m_parameters.sensor.measurementIntervalMs >= 0) {
    m_measurementIntervalMs = std::clamp(static_cast<std::uint32_t>(m_parameters.sensor.measurementIntervalMs),
                                         MIN_MEASUREMENT_INTERVAL_MS, MAX_MEASUREMENT_INTERVAL_MS);
    m_logger(LogLevel::INFO, "Measurement interval is set to ", m_measurementIntervalMs, " ms");
  } else {
    m_logger(LogLevel::INFO, "Measurement interval defaults to ", m_measurementIntervalMs, " ms until configured");
  }
}

void Sensor::doLoop() {
  Core::doLoop();

  if (!isReady())
    return;

  const std::uint32_t currentLoopStart = millis();
  if (currentLoopStart - m_previousMeasurementPointMs < m_measurementIntervalMs && m_previousMeasurementPointMs > 0)
    return;

  if (!m_sensor.read(true)) {
    m_logger(LogLevel::ERROR, "Failed to read the sensor");

    if (m_isLastSensorReadSuccessful) {
      m_isLastSensorReadSuccessful = false;
      publishAvailability(false);
    }

    m_led.toggle();
    delay(SENSOR_FAILURE_LED_PERIOD_MS / 2);

    return;
  }

  if (!m_isLastSensorReadSuccessful) {
    m_logger(LogLevel::INFO, "Sensor is back online");
    m_isLastSensorReadSuccessful = true;
    publishAvailability(true);
    m_led.turnOff();
  }

  readSensorState();
  publishStateMessage();
  m_previousMeasurementPointMs = currentLoopStart;
}

void Sensor::setupSensor() { m_sensor.begin(); }

void Sensor::publishDiscoveryMessages() {
  publishTemperatureDiscoveryMessage();
  publishHumidityDiscoveryMessage();
  publishMeasurementIntervalDiscoveryMessage();

  delay(500);
}

void Sensor::publishTemperatureDiscoveryMessage() {
  m_logger(LogLevel::INFO, "Sending a temperature discovery message");

  ArduinoJson::JsonDocument config;

  config["name"] = "Temperature";
  config["unique_id"] = m_parameters.name + "_temperature";
  config["object_id"] = m_parameters.name + "_temperature";

  config["availability_topic"] = m_availabilityTopic;
  config["state_topic"] = m_stateTopic;

  config["device_class"] = "temperature";
  config["unit_of_measurement"] = "°C";
  config["suggested_display_precision"] = 1;

  config["force_update"] = true;
  config["value_template"] = "{{ value_json.temperature }}";
  config["device"] = makeDeviceJson();

  publishJson(m_temperatureDiscoveryTopic, config);
}

void Sensor::publishHumidityDiscoveryMessage() {
  m_logger(LogLevel::INFO, "Sending a humidity discovery message");

  ArduinoJson::JsonDocument config;

  config["name"] = "Humidity";
  config["unique_id"] = m_parameters.name + "_humidity";
  config["object_id"] = m_parameters.name + "_humidity";

  config["availability_topic"] = m_availabilityTopic;
  config["state_topic"] = m_stateTopic;

  config["device_class"] = "humidity";
  config["unit_of_measurement"] = "%";
  config["suggested_display_precision"] = 1;

  config["force_update"] = true;
  config["value_template"] = "{{ value_json.humidity }}";
  config["device"] = makeDeviceJson();

  publishJson(m_humidityDiscoveryTopic, config);
}

void Sensor::publishMeasurementIntervalDiscoveryMessage() {
  m_logger(LogLevel::INFO, "Sending a measurement interval command discovery message");

  ArduinoJson::JsonDocument config;

  config["name"] = "Measurement Interval";
  config["unique_id"] = m_parameters.name + "_measurement_interval";
  config["object_id"] = m_parameters.name + "_measurement_interval";

  config["availability_topic"] = m_availabilityTopic;
  config["command_topic"] = m_measurementIntervalCommandTopic;
  config["state_topic"] = m_stateTopic;

  config["unit_of_measurement"] = "ms";
  config["step"] = 100;

  if (m_parameters.sensor.measurementIntervalMs < 0) {
    config["min"] = MIN_MEASUREMENT_INTERVAL_MS;
    config["max"] = MAX_MEASUREMENT_INTERVAL_MS;
  } else {
    // Prevent setting this value through MQTT
    config["min"] = m_parameters.sensor.measurementIntervalMs;
    config["max"] = m_parameters.sensor.measurementIntervalMs + 1;
  }

  config["mode"] = "box";
  config["force_update"] = true;
  config["value_template"] = "{{ value_json.measurement_interval_ms }}";
  config["retain"] = true;
  config["device"] = makeDeviceJson();

  publishJson(m_measurementIntervalDiscoveryTopic, config);
}

void Sensor::readSensorState() {
  m_lastReadTemperature = m_sensor.readTemperature();
  m_lastReadHumidity = m_sensor.readHumidity();

  m_logger(LogLevel::DEBUG, "Measured temperature is: ", m_lastReadTemperature, "°C");
  m_logger(LogLevel::DEBUG, "Measured humidity is: ", m_lastReadHumidity, "%");
}

void Sensor::publishStateMessage() {
  m_logger(LogLevel::INFO, "Sending state message");

  ArduinoJson::JsonDocument state;
  state["temperature"] = m_lastReadTemperature;
  state["humidity"] = m_lastReadHumidity;
  state["measurement_interval_ms"] = m_measurementIntervalMs;

  publishJson(m_stateTopic, state);
}

void Sensor::publishAvailability(bool isOnline) {
  if (m_isLastSensorReadSuccessful) {
    Core::publishAvailability(isOnline);
  } else {
    Core::publishAvailability(false);
  }
}

void Sensor::doSubscriptionCallback(std::string_view topic, std::string_view payload) {
  Core::doSubscriptionCallback(topic, payload);

  if (topic == m_measurementIntervalCommandTopic) {
    std::uint32_t newIntervalMs{};
    const auto result = std::from_chars(payload.data(), payload.data() + payload.size(), newIntervalMs);

    if (result.ec == std::errc{} && result.ptr == payload.data() + payload.size()) {
      if (m_parameters.sensor.measurementIntervalMs < 0) {
        m_measurementIntervalMs = std::clamp(newIntervalMs, MIN_MEASUREMENT_INTERVAL_MS, MAX_MEASUREMENT_INTERVAL_MS);
        m_logger(LogLevel::INFO, "Measurement interval is set to ", m_measurementIntervalMs, " ms");

        publishStateMessage();
      } else {
        m_logger(LogLevel::WARNING, "Measurement interval is fixed in configuration");
      }
    } else {
      m_logger(LogLevel::ERROR, "Received a value that is not a number");
    }
  }
}

void Sensor::doReconnectCallback() {
  Core::doReconnectCallback();
  subscribe(m_measurementIntervalCommandTopic);
}

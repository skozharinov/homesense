#ifdef HOMESENSE_DISPLAY_TARGET
#include "Components/Display.hpp"
using Target = HomeSense::Display;
#endif

#ifdef HOMESENSE_SENSOR_TARGET
#include "Components/Sensor.hpp"
using Target = HomeSense::Sensor;
#endif

void setup() {
  Target::createInstance(HomeSense::readParameters());
  Target::getInstance().doSetup();
}

void loop() { Target::getInstance().doLoop(); }

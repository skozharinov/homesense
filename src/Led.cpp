#include "Led.hpp"

#include <Arduino.h>

using namespace HomeSense;

Led::Led(std::uint8_t pin, bool isInverted)
    : m_pin(pin), m_onState(isInverted ? LOW : HIGH), m_offState(isInverted ? HIGH : LOW) {}

void Led::doSetup() { pinMode(m_pin, OUTPUT); }

void Led::turnOn() { digitalWrite(m_pin, m_onState); }

void Led::turnOff() { digitalWrite(m_pin, m_offState); }

void Led::toggle() { isOn() ? turnOff() : turnOn(); }

bool Led::isOn() const { return digitalRead(m_pin) == m_onState; }

bool Led::isOff() const { return digitalRead(m_pin) == m_offState; }

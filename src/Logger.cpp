#include "Logger.hpp"

using namespace HomeSense;

std::string_view HomeSense::getLevelStr(LogLevel level) {
  switch (level) {
  case LogLevel::FATAL:
    return "FATAL";
  case LogLevel::ERROR:
    return "ERROR";
  case LogLevel::WARNING:
    return "WARNING";
  case LogLevel::INFO:
    return "INFO";
  case LogLevel::DEBUG:
    return "DEBUG";
  default:
    return "";
  }
}

Logger::Logger(LogLevel level) : m_level(level) {}

void Logger::doSetup() {
  if (m_level < LogLevel::NONE)
    m_serial.begin(115200);
}

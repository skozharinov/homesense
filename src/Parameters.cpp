#include "Parameters.hpp"

#include <ArduinoJson.hpp>

#include <LittleFS.h>

namespace HomeSense {
bool canConvertFromJson(ArduinoJson::JsonVariantConst json, const LogLevel &) {
  const auto str = json.as<std::string>();
  return str == "DEBUG" || str == "INFO" || str == "WARNING" || str == "ERROR" || str == "FATAL" || str == "NONE";
}

void convertFromJson(ArduinoJson::JsonVariantConst json, LogLevel &level) {
  const auto str = json.as<std::string>();

  if (str == "DEBUG")
    level = LogLevel::DEBUG;
  else if (str == "INFO")
    level = LogLevel::INFO;
  else if (str == "WARNING")
    level = LogLevel::WARNING;
  else if (str == "ERROR")
    level = LogLevel::ERROR;
  else if (str == "FATAL")
    level = LogLevel::FATAL;
  else
    level = LogLevel::NONE;
}

void convertFromJson(ArduinoJson::JsonVariantConst json, LogParameters &parameters) {
  parameters.level = json["level"] | parameters.level;
}

void convertFromJson(ArduinoJson::JsonVariantConst json, WifiParameters &parameters) {
  parameters.ssid = json["ssid"] | parameters.ssid;
  parameters.password = json["password"] | parameters.password;
}

void convertFromJson(ArduinoJson::JsonVariantConst json, MqttParameters &parameters) {
  parameters.host = json["host"] | parameters.host;
  parameters.port = json["port"] | parameters.port;
  parameters.user = json["user"] | parameters.user;
  parameters.password = json["password"] | parameters.password;
}

void convertFromJson(ArduinoJson::JsonVariantConst json, OtaParameters &parameters) {
  parameters.password = json["password"].as<std::string>();
}

#ifdef HOMESENSE_SENSOR_TARGET
bool canConvertFromJson(ArduinoJson::JsonVariantConst json, const SensorKind &) {
  const auto str = json.as<std::string>();
  return str == "DHT11" || str == "DHT12" || str == "DHT21" || str == "DHT22";
}

void convertFromJson(ArduinoJson::JsonVariantConst json, SensorKind &kind) {
  const auto str = json.as<std::string>();

  if (str == "DHT11")
    kind = SensorKind::DHT11;
  else if (str == "DHT12")
    kind = SensorKind::DHT12;
  else if (str == "DHT21")
    kind = SensorKind::DHT21;
  else if (str == "DHT22")
    kind = SensorKind::DHT22;
  else
    kind = SensorKind::NONE;
}

void convertFromJson(ArduinoJson::JsonVariantConst json, SensorParameters &parameters) {
  parameters.pin = json["pin"] | parameters.pin;
  parameters.kind = json["kind"] | parameters.kind;
  parameters.measurementIntervalMs = json["measurementIntervalMs"] | parameters.measurementIntervalMs;
}
#endif

#ifdef HOMESENSE_DISPLAY_TARGET
void convertFromJson(ArduinoJson::JsonVariantConst json, DisplayParameters &parameters) {
  parameters.dataPin = json["dataPin"] | parameters.dataPin;
  parameters.clockPin = json["clockPin"] | parameters.clockPin;
  parameters.width = json["width"] | parameters.width;
  parameters.height = json["height"] | parameters.height;
}
#endif

void convertFromJson(ArduinoJson::JsonVariantConst json, Parameters &parameters) {
  parameters.name = json["name"] | parameters.name;

  parameters.log = json["log"].as<LogParameters>();
  parameters.wifi = json["wifi"].as<WifiParameters>();
  parameters.mqtt = json["mqtt"].as<MqttParameters>();
  parameters.ota = json["ota"].as<OtaParameters>();

#ifdef HOMESENSE_SENSOR_TARGET
  parameters.sensor = json["sensor"].as<SensorParameters>();
#endif
#ifdef HOMESENSE_DISPLAY_TARGET
  parameters.display = json["display"].as<DisplayParameters>();
#endif
}
} // namespace HomeSense

using namespace HomeSense;

Parameters HomeSense::readParameters() {
  LittleFS.begin();
  File file = LittleFS.open("/config.json", "r");

  ArduinoJson::JsonDocument json;
  deserializeJson(json, file);

  file.close();

  return json.as<Parameters>();
}
